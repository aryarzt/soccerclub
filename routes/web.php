<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClubController;
use App\Http\Controllers\ScoreController;
use App\Http\Controllers\StandingsController;
use App\Http\Controllers\MatchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/master', function () {
    return view('layouts.master');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/clubs', ClubController::class);
Route::resource('/scores', ScoreController::class);
Route::get('/standings', [StandingsController::class, 'index'])->name('standings.index');
Route::get('/matches/create', [MatchController::class, 'create'])->name('matches.create');
Route::resource('/matches', MatchController::class);
// Route::post('/matches/multiple', [MatchController::class, 'storeMultiple'])->name('matches.storeMultiple');
// Route::post('/matches/multiple', [MatchController::class, 'storeMultiple'])->name('matches.storeMultiple');