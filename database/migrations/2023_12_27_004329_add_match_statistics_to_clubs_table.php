<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMatchStatisticsToClubsTable extends Migration
{
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->unsignedInteger('match_played')->default(0);
            $table->unsignedInteger('match_win')->default(0);
            $table->unsignedInteger('match_lose')->default(0);
            $table->unsignedInteger('match_draw')->default(0);
            $table->unsignedInteger('goals_match_win')->default(0);
            $table->unsignedInteger('goals_match_lose')->default(0);
        });
    }

    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn(['played', 'won', 'drawn', 'lost', 'goals_for']);
        });
    }
}
