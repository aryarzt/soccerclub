<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoresTable extends Migration
{
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('club_id')->constrained();
            $table->integer('match_played');
            $table->integer('match_win');
            $table->integer('match_lose');
            $table->integer('match_draw');
            $table->integer('goals_match_win');
            $table->integer('goals_match_lose');
            $table->integer('points');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('scores');
    }
}