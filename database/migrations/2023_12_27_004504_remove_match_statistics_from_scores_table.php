<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveMatchStatisticsFromScoresTable extends Migration
{
    public function up()
    {
        Schema::table('scores', function (Blueprint $table) {
            $table->dropColumn(['match_played', 'match_win', 'match_lose', 'match_draw', 'goals_match_win','goals_match_lose','points']);
            // hapus kolom lain jika perlu
        });
    }

    public function down()
    {
        Schema::table('scores', function (Blueprint $table) {
            $table->unsignedInteger('match_played')->default(0);
            $table->unsignedInteger('match_win')->default(0);
            $table->unsignedInteger('match_lose')->default(0);
            $table->unsignedInteger('match_draw')->default(0);
            $table->unsignedInteger('goals_match_win')->default(0);
            $table->unsignedInteger('goals_match_lose')->default(0);
            $table->unsignedInteger('points')->default(0);
            // tambahkan kolom lain jika perlu
        });
    }
}
