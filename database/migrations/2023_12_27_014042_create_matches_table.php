<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('club_1_id');
            $table->unsignedBigInteger('club_2_id');
            $table->string('score');
            $table->integer('club_1_goals')->default(0);
            $table->integer('club_2_goals')->default(0);
            // tambahkan kolom-kolom lain yang dibutuhkan
            $table->timestamps();

            $table->foreign('club_1_id')->references('id')->on('clubs')->onDelete('cascade');
            $table->foreign('club_2_id')->references('id')->on('clubs')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
