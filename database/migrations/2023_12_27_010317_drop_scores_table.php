<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropScoresTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('scores');
    }

    public function down()
    {

    }
}
