<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPointsToClubs extends Migration
{
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->integer('points')->default(0);
        });
    }

    public function down()
    {
        
    }
}