@extends('layouts.content')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Matches</div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Club 1</th>
                                    <th>Club 2</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($matches as $match)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $match->club1->name }}</td>
                                        <td>{{ $match->club2->name }}</td>
                                        <td>
                                            <a href="{{ route('matches.show', $match->id) }}" class="btn btn-primary">View Details</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection