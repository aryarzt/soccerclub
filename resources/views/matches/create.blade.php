@extends('layouts.content')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Match</div>

                    <div class="card-body">
                        <form action="{{ route('matches.store') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="club_1_id">Club 1:</label>
                                <select class="form-control" name="club_1_id" id="club_1_id">
                                    @foreach($clubs as $club)
                                        <option value="{{ $club->id }}">{{ $club->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="club_2_id">Club 2:</label>
                                <select class="form-control" name="club_2_id" id="club_2_id">
                                    @foreach($clubs as $club)
                                        <option value="{{ $club->id }}">{{ $club->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="score">Score:</label>
                                <input type="text" class="form-control" name="score" id="score">
                            </div>

                            <div class="form-group">
                                <label for="club_1_goals">Club 1 Goals:</label>
                                <input type="number" class="form-control" name="club_1_goals" id="club_1_goals">
                            </div>

                            <div class="form-group">
                                <label for="club_2_goals">Club 2 Goals:</label>
                                <input type="number" class="form-control" name="club_2_goals" id="club_2_goals">
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection