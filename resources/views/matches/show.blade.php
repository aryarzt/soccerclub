@extends('layouts.content')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Match Details</div>

                    <div class="card-body">
                        <h4>Club 1: {{ $match->club1->name }}</h4>
                        <h4>Club 2: {{ $match->club2->name }}</h4>
                        <p>Score: {{ $match->score }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection