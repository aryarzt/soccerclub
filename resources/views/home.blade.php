@extends('layouts.content')

@section('title', 'Dashboard')

@section('content')

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h2 class="mb-4">Menu</h2>
            <div class="list-group">
                <a href="{{ route('clubs.index') }}" class="list-group-item list-group-item-action">View Club</a>
                
                <a href="{{ route('matches.index') }}" class="list-group-item list-group-item-action">View Match</a>
                
                <a href="{{ route('standings.index') }}" class="list-group-item list-group-item-action">View Standings</a>
                
                <a href="{{ route('clubs.create') }}" class="list-group-item list-group-item-action">Create Club</a>
                
                <a href="{{ route('matches.create') }}" class="list-group-item list-group-item-action">Create Match</a>
            </div>
        </div>
    </div>
</div>

@endsection