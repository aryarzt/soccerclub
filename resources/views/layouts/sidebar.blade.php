<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
      <div class="sidebar-brand-icon">
        <img src="https://static.vecteezy.com/system/resources/previews/023/579/944/original/illustration-of-soccer-logo-it-s-for-success-concept-png.png">
      </div>
      <div class="sidebar-brand-text mx-3">Soccer Match</div>
    </a>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
      Features
    </div>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseClub"
        aria-expanded="true" aria-controls="collapseClub">
        <i class="far fa-fw fa-window-maximize"></i>
        <span>Club</span>
      </a>
      <div id="collapseClub" class="collapse" aria-labelledby="headingClub" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Club</h6>
          <a class="collapse-item" href="/clubs/create">Add Club</a>
          <a class="collapse-item" href="/clubs">View All Club</a>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collageMatches"
        aria-expanded="true" aria-controls="collageMatches">
        <i class="far fa-fw fa-window-maximize"></i>
        <span>Match</span>
      </a>
      <div id="collageMatches" class="collapse" aria-labelledby="headingMatches" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Match</h6>
          <a class="collapse-item" href="/matches/create">Add Match</a>
          <a class="collapse-item" href="/matches">View All Match</a>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/standings">
        <i class="fa fa-briefcase"></i>
        <span>Standings</span>
      </a>
    </li>
    <hr class="sidebar-divider">
    <div class="version" id="version-ruangadmin"></div>
  </ul>