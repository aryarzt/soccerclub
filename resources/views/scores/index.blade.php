@extends('layouts.content')


@section('content')

<div class="container">
    <h1>Scores</h1>
    <a href="{{ route('scores.create') }}" class="btn btn-primary mb-3">Add Score</a>

    @if($scores->isEmpty())
        <p>No scores available.</p>
    @else
        <table class="table">
            <thead>
                <tr>
                    <th>Club Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($scores as $score)
                    <tr>
                        <td>{{ $score->club->name }}</td>
                        <td>
                            <a href="{{ route('scores.show', $score->id) }}" class="btn btn-sm btn-info">View</a>
                            <a href="{{ route('scores.edit', $score->id) }}" class="btn btn-sm btn-primary">Edit</a>
                            <form action="{{ route('scores.destroy', $score->id) }}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this score?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>

@endsection