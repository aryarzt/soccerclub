@extends('layouts.content')

@section('content')

    <div class="container">
        <h1>Score Details</h1>

        <div class="card">
            <div class="card-body">
                <p class="card-text"><strong>Club Name : </strong> {{ $score->club->name }}</p>
                <p class="card-text"><strong>Match Played: </strong> {{ $score->match_played }}</p>
                <p class="card-text"><strong>Match Win : </strong> {{ $score->match_win }}</p>
                <p class="card-text"><strong>Match Lose : </strong> {{ $score->match_lose }}</p>
                <p class="card-text"><strong>Match Draw :</strong> {{ $score->match_draw }}</p>
                <p class="card-text"><strong>Goals When Match Win: </strong> {{ $score->goals_match_win }}</p>
                <p class="card-text"><strong>Goals When Match Lose : </strong> {{ $score->goals_match_lose }}</p>
                <p class="card-text"><strong>Points : </strong> {{ $score->points }}</p>
                
                <a href="{{ url()->previous() }}" class="btn btn-secondary">Back</a>
            </div>
        </div>
    </div>

@endsection
