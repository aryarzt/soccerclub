@extends('layouts.content')

@section('content')

<div class="container">
    <h1>Edit Score</h1>
    <form action="{{ route('scores.update', $score->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="club_id">Club Name:</label>
            <input type="text" value="{{ $score->club->name }}" class="form-control" readonly>
        </div>

        <div class="form-group">
            <label for="match_played">Match Played</label>
            <input type="number" name="match_played" value="{{ $score->match_played }}" class="form-control">
        </div>

        <div class="form-group">
            <label for="match_win">Match Win:</label>
            <input type="number" name="match_win" value="{{ $score->match_win }}" class="form-control">
        </div>

        <div class="form-group">
            <label for="match_lose">Match Lose:</label>
            <input type="number" name="match_lose" value="{{ $score->match_lose }}" class="form-control">
        </div>

        <div class="form-group">
            <label for="match_draw">Match Draw:</label>
            <input type="number" name="match_draw" value="{{ $score->match_draw }}" class="form-control">
        </div>

        <div class="form-group">
            <label for="goals_match_win">Goals When Match Win:</label>
            <input type="number" name="goals_match_win" value="{{ $score->goals_match_win }}" class="form-control">
        </div>

        <div class="form-group">
            <label for="goals_match_lose">Goals When Match Lose:</label>
            <input type="number" name="goals_match_lose" value="{{ $score->goals_match_lose }}" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">Update Score</button>
    </form>
</div>

@endsection