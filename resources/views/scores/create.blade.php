@extends('layouts.content')

@section('content')

    <div class="container">
        <h1>Add Score</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('scores.store') }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="club_id">Club:</label>
                <select name="club_id" class="form-control">
                    @foreach ($clubs as $club)
                        <option value="{{ $club->id }}">{{ $club->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="played">Match Played:</label>
                <input type="number" name="match_played" class="form-control" required>
            </div>

            <div class="form-group">
                <label for="won">Match Win:</label>
                <input type="number" name="match_win" class="form-control" required>
            </div>

            <div class="form-group">
                <label for="lost">Match Lose:</label>
                <input type="number" name="match_lose" class="form-control" required>
            </div>

            <div class="form-group">
                <label for="drawn">Match Draw:</label>
                <input type="number" name="match_draw" class="form-control" required>
            </div>

            <div class="form-group">
                <label for="goals_for">Goals When Match Win:</label>
                <input type="number" name="goals_match_win" class="form-control" required>
            </div>

            <div class="form-group">
                <label for="goals_against">Goals When Match Lose:</label>
                <input type="number" name="goals_match_lose" class="form-control" required>
            </div>

            <div class="form-group">
                <label for="points">Points:</label>
                <input type="number" name="points" class="form-control" required>
            </div>

            <button type="submit" class="btn btn-primary">Add Score</button>
        </form>
    </div>
@endsection
