@extends('layouts.content')

@section('content')
<div class="container">
    <h1>Standings</h1>
    <table class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>Club</th>
                <th>Played</th>
                <th>Win</th>
                <th>Lose</th>
                <th>Draw</th>
                <th>Goals Win</th>
                <th>Goals Lose</th>
            </tr>
        </thead>
        <tbody>
            @foreach($standings as $index => $standing)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $standing->club->name }}</td>
                    <td>{{ $standing->played }}</td>
                    <td>{{ $standing->win }}</td>
                    <td>{{ $standing->lose }}</td>
                    <td>{{ $standing->draw }}</td>
                    <td>{{ $standing->goals_win }}</td>
                    <td>{{ $standing->goals_lose }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection