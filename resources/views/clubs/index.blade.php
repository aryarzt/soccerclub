@extends('layouts.content')

@section('content')

<div class="container mt-4">
    <h1>Club List</h1>

    <a href="{{ route('clubs.create') }}" class="btn btn-primary mb-3">Add Club</a>

    @if (count($clubs) > 0)
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clubs as $club)
                    <tr>
                        <td>{{ $club->name }}</td>
                        <td>{{ $club->city }}</td>
                        <td>
                            <a href="{{ route('clubs.edit', $club->id) }}" class="btn btn-sm btn-primary">Edit</a>
                            <form action="{{ route('clubs.destroy', $club->id) }}" method="post" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this club?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>No clubs available.</p>
    @endif
</div>

@endsection