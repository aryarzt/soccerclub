@extends('layouts.content')

@section('content')

<div class="container mt-4">
    <h1>Edit Club</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('clubs.update', $club->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" value="{{ $club->name }}" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="city">City:</label>
            <input type="text" name="city" value="{{ $club->city }}" class="form-control" required>
        </div>

        <button type="submit" class="btn btn-primary">Update Club</button>
    </form>
</div>

@endsection