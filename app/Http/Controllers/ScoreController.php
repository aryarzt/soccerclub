<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Score;
use App\Models\Club;
use Illuminate\Support\Facades\Validator;

class ScoreController extends Controller
{
    public function index()
    {
        $scores = Score::all();
        return view('scores.index', compact('scores'));
    }

    public function create()
    {
        $clubs = Club::all();
        return view('scores.create', compact('clubs'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'club1' => 'required|different:club2',
            'club2' => 'required',
            'match_played' => 'required|numeric',
            'match_win' => 'required|numeric',
            'match_lose' => 'required|numeric',
            'match_draw' => 'required|numeric',
            'goals_match_win' => 'required|numeric',
            'goals_match_lose' => 'required|numeric',
        ]);

        $score = new Score();
        $score->club1_id = $request->input('club1');
        $score->club2_id = $request->input('club2');
        $score->played = $request->input('played');
        $score->match_win = $request->input('won');
        $score->match_draw = $request->input('drawn');
        $score->match_lst = $request->input('lost');
        $score->goals_for = $request->input('goals_for');

        $score->save();

        return redirect()->route('scores.index')->with('success', 'Score created successfully');
    }

    public function show($id)
    {
        $score = Score::findOrFail($id);
        return view('scores.show', compact('score'));
    }

    public function edit($id)
    {
        $score = Score::findOrFail($id);
        $clubs = Club::all();
        return view('scores.edit', compact('score', 'clubs'));
    }
    
    public function update(Request $request, $id)
    {
        $score = Score::findOrFail($id);
        $score->update($request->all());
    
        return redirect()->route('scores.index')->with('success', 'Score updated successfully');
    }

    public function destroy($id)
    {
        $score = Score::findOrFail($id);
        $score->delete();

        return redirect()->route('scores.index')->with('success', 'Score deleted successfully');
    }
}
