<?php

namespace App\Http\Controllers;

use App\Models\Match;
use App\Models\Club;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    public function create()
    {
        $clubs = Club::all(); 

        return view('matches.create', compact('clubs'));
    }

    public function store(Request $request)
    {
        $match = Match::create([
            'club_1_id' => $request->club_1_id,
            'club_2_id' => $request->club_2_id,
            'score' => $request->score,
            'club_1_goals' => $request->club_1_goals,
            'club_2_goals' => $request->club_2_goals,
        ]);

        if ($match->club_1_goals > $match->club_2_goals) {
            $club1 = Club::find($match->club_1_id);
            $club2 = Club::find($match->club_2_id);
            $club1->match_win += 1;
            $club1->match_played += 1;
            $club1->match_lose += 0;
            $club1->goals_match_win += $request->club_1_goals;
            $club1->goals_match_lose += 0;
            $club1->points += 3;
            $club2->match_played += 1;
            $club2->match_lose += 1;
            $club2->goals_match_lose += $request->club_2_goals;
            $club1->save();
            $club2->save();
    
        } elseif ($match->club_1_goals < $match->club_2_goals) {
            $club2 = Club::find($match->club_1_id);
            $club1 = Club::find($match->club_2_id);
            $club2->match_win += 1;
            $club2->match_played += 1;
            $club2->match_lose += 0;
            $club2->goals_match_win += $request->club_2_goals;
            $club2->goals_match_lose += 0;
            $club2->points += 3;
            $club1->match_played += 1;
            $club1->match_lose += 1;
            $club1->goals_match_lose += $request->club_1_goals;
            $club1->save();
            $club2->save();

        } else {
            $club1 = Club::find($match->club_1_id);
            $club2 = Club::find($match->club_1_id);
            $club1->points += 1;
            $club2->points += 1;
            $club1->save();
            $club2->save();
        }

        return redirect()->route('matches.index');
    }

    public function index()
    {
        $matches = Match::all();
        return view('matches.index', compact('matches'));
    }

    public function show($id)
    {
        $match = Match::findOrFail($id);
        return view('matches.show', compact('match'));
    }
}