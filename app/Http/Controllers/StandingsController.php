<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\Score;
use App\Models\Match;

class StandingsController extends Controller
{
    public function index()
    {
        // Ambil semua klub
        $clubs = Club::all();
    
        // Hitung statistik dan tambahkan ke dalam array
        $standings = [];
        foreach ($clubs as $club) {
            $stats = [
                'club' => $club,
                'played' => $club->match_played,
                'win' => $club->match_win,
                'lose' => $club->match_lose,
                'draw' => $club->match_draw,
                'goals_win' => $club->goals_match_win,
                'goals_lose' => $club->goals_match_lose,
            ];
    
            $standings[] = (object) $stats;
        }
    
        // Urutkan klasemen berdasarkan poin
        usort($standings, function ($a, $b) {
            return $b->win * 3 + $b->draw - ($a->win * 3 + $a->draw);
        });
    
        return view('standings.index', compact('standings'));
    }
}