<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Club;
use App\Models\Score;
use Illuminate\Support\Facades\Validator;

class ClubController extends Controller
{
    public function index()
    {
        $clubs = Club::all();
        return view('clubs.index', compact('clubs'));
    }

    public function create()
    {
        return view('clubs.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:clubs,name,' . $request->id,
            'city' => 'required',
        ], [
            'name.unique' => 'Club is already exist.',
        ]);
    
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
    
        $club = Club::firstOrCreate(
            ['name' => $request->input('name')],
            ['city' => $request->input('city')]
        );
    

    
        return redirect()->route('clubs.index');
    }

    public function show($id)
    {
        $club = Club::findOrFail($id);
        $score = $club->score;
        return view('clubs.show', compact('club', 'score'));
    }

    public function edit($id)
    {
    $club = Club::findOrFail($id);
    return view('clubs.edit', compact('club'));
    }

    public function update(Request $request, $id)
{
    $club = Club::findOrFail($id);
    $club->update($request->only(['name', 'city']));

    return redirect()->route('clubs.index')->with('success', 'Club updated successfully');
}

public function destroy($id)
{
    $club = Club::findOrFail($id);
    $club->delete();

    return redirect()->route('clubs.index')->with('success', 'Club deleted successfully');
}

}