<?php

// app/Models/Match.php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use HasFactory;

    protected $fillable = [
        'club_1_id',
        'club_2_id',
        'score',
        'club_1_goals',
        'club_2_goals',
    ];

    // Definisikan relasi ke model Club
    public function club1()
    {
        return $this->belongsTo(Club::class, 'club_1_id');
    }

    public function club2()
    {
        return $this->belongsTo(Club::class, 'club_2_id');
    }
}
