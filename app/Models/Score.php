<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = [
    'club1_id',
    'club2_id',
    'match_played',
    'match_win',
    'match_lose',
    'match_draw',
    'goals_match_win',
    'goals_match_lose',
    'points',
];

    public function club1()
    {
        return $this->belongsTo(Club::class, 'club1_id');
    }

    public function club2()
    {
        return $this->belongsTo(Club::class, 'club2_id');
    }
}
